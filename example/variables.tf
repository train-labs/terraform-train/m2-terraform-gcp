variable "project" { 
    default = "<PROJECT_ID>"
}

variable "credentials_file" {
    default = "credentials.json"
 }

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-c"
}