## Terraform config
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.0.0"
    }
  }
}

## Providers
provider "google" {
  credentials = file(var.credentials_file)

  project = var.project
  region  = var.region
  zone    = var.zone
}

## Resources
resource "google_compute_network" "vpc_network" {
  name = "my-terraform-network"
}