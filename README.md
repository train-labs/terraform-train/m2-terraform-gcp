# 1 - INSTALAR TERRAFORM
```
# Preparar componentes

$ sudo su
$ apt-get update
$ apt-get install wget unzip
$ wget https://releases.hashicorp.com/terraform/1.0.11/terraform_1.0.11_linux_amd64.zip
$ unzip terraform_1.0.11_linux_amd64.zip

# Print a colon-separated list of locations in your PATH.

$ echo $PATH
$ mv ./terraform /usr/local/bin/


```

Verificar instalación

```
Verify that the installation worked by opening a new terminal session and listing Terraform's available subcommands.

$ terraform -help
$ rm terraform_1.0.11_linux_amd64.zip
```

# 2 - PREPARAR ENTORNO DE TRABAJO 

## Prerequisites

> Cuenta -  Google Cloud Platform .

> Terraform 0.15.3+ installed locally.

## Configurar GCP

- **GCP Project**: (OK)

- **Google Compute Engine**: Habilitar *"Google Compute Engine"* (https://console.cloud.google.com/apis/library/compute.googleapis.com) (OK)

- **Crear una clave de cuenta de servicio de GCP**: cree una clave de cuenta de servicio para permitir que Terraform acceda a su cuenta de GCP. Al crear la clave, utilice la siguiente configuración:

> ### Crear cuenta de servicio:

> **I) Seleccione el proyecto que creó en el paso anterior.**

- Haga clic en "IAM" -> "Service account" -> "Create service account".

- Asígnele el nombre que desee y haga clic en "Create and continue".

- Para el rol, elija "Basic -> Editor", luego haga clic en "Continue".

- Omita la concesión de acceso a usuarios adicionales y haga clic en "Done".

> **II) Después de crear su cuenta de servicio, descargue su clave de cuenta de servicio.**

- Seleccione su cuenta de servicio de la lista.

- Seleccione la pestaña "Keys".

- En el menú desplegable, seleccione "ADD KEY" -> "Create new key".

- Deje el "Key Type" como JSON.

- Haga clic en "Crear" para crear la clave y guardar el archivo de la clave en su sistema.

# 3 - PREPARAR CONFIGURACIONES - TERRAFORM

3.1 - Cada configuración de Terraform debe estar en su propio directorio de trabajo. Cree un directorio para su configuración. 
```
$ mkdir labs-terraform-gcp

# Cambiamos al dirctorio
$ cd labs-terraform-gcp
```

3.2 - Terraform carga todos los archivos que terminan en .tf o .tf.json en el directorio de trabajo. Cree un archivo main.tf para su configuración y mueva la cuanta de servicio creada anteriormente al directorio.

```
Crear archivo main que utilizaremos mas adelante, será la base de nuestras configuraciones de terraform.
$ touch main.tf

# Edite el contenido de "credentials.json" y copie el contenido de la cuenta de servicio

$ touch credentials.json
$ nano credentials.json 

# Copie el contenido de archio json de la cuenta de servicio y guardar con "ctrl + o"

```

Abra main.tf en su editor de texto y pegue la configuración a continuación. Asegúrese de reemplazar <NAME> con la ruta al archivo de clave de la cuenta de servicio que descargó y <PROJECT_ID> con la ID de su proyecto, y guarde el archivo. 


```
## Terraform config
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.0.0"
    }
  }
}

## Providers
provider "google" {
  credentials = file("<NAME>.json")

  project = "<PROJECT_ID>"
  region  = "us-central1"
  zone    = "us-central1-c"
}

## Resources
resource "google_compute_network" "vpc_network" {
  name = "my-terraform-network"
}
```

### Terraform Block

El bloque terraform {} contiene la configuración de Terraform, incluidos los proveedores necesarios que Terraform utilizará para aprovisionar su infraestructura. Para cada proveedor, el atributo de origen define un nombre de host opcional, un espacio de nombres y el tipo de proveedor. Terraform instala proveedores de Terraform Registry de forma predeterminada. En esta configuración de ejemplo, la fuente del proveedor de Google se define como 'hashicorp/google', que es la abreviatura de registry.terraform.io/hashicorp/google.

También puede definir una restricción de versión para cada proveedor en el bloque required_providers. El atributo de versión es opcional, pero recomendamos usarlo para hacer cumplir la versión del proveedor. Sin él, Terraform siempre usará la última versión del proveedor, lo que puede introducir cambios importantes.

Terraform registry GCP: https://registry.terraform.io/providers/hashicorp/google/latest

### Resources
Utilice bloques de recursos para definir componentes de su infraestructura. Un recurso puede ser un componente físico, como un servidor, o puede ser un recurso lógico, como una aplicación Heroku.

Los bloques de recursos tienen dos cadenas antes del bloque: el tipo de recurso y el nombre del recurso. En este ejemplo, el tipo de recurso es *google_compute_network* y el nombre es vpc_network. El prefijo del tipo se asigna al nombre del proveedor. En la configuración de ejemplo, Terraform administra el recurso google_compute_network con el proveedor de Google. Juntos, el tipo de recurso y el nombre del recurso forman una identificación única para el recurso. Por ejemplo, el ID de su red es google_compute_network.vpc_network.

Los bloques de recursos contienen argumentos que se utilizan para configurar el recurso. Los argumentos pueden incluir cosas como tamaños de máquina, nombres de imágenes de disco o ID de VPC. La página de documentación de Terraform Registry GCP documenta los argumentos obligatorios y opcionales para cada recurso de GCP. Por ejemplo, puede leer la documentación de google_compute_network para ver los argumentos admitidos y los atributos disponibles del recurso.

El proveedor de GCP documenta los recursos admitidos, incluidos google_compute_network y sus argumentos admitidos.

# 4 - INICIALIZAR DIRECTORIO

Cuando crea una nueva configuración, o comprueba una configuración existente desde el control de versiones, debe inicializar el directorio con terraform init. Este paso descarga los proveedores definidos en la configuración.

Inicialice el directorio.

```
$ terraform init

```

# 5 - FORMATEE Y VALIDE LA CONFIGURACION 

Recomendamos utilizar un formato coherente en todos sus archivos de configuración. El comando ```terraform fmt ``` actualiza automáticamente las configuraciones en el directorio actual para mejorar la legibilidad y la coherencia.

Formatee su configuración. Terraform imprimirá los nombres de los archivos que modificó, si los hay. En este caso, su archivo de configuración ya estaba formateado correctamente, por lo que Terraform no devolverá ningún nombre de archivo.

También puede asegurarse de que su configuración sea sintácticamente válida y coherente internamente utilizando el comando terraform validate.
```
$ terraform fmt

```

Valida tu configuración. 

```
$ terraform validate

```

# 6 - CREAR INFRAESTRUCTURA

Aplique la configuración ahora con el comando ```terraform apply```. Terraform imprimirá una salida similar a la que se muestra a continuación.

```
$ terraform apply

```

# 7 - INSPECCIONAR ESTADO
```
$ terraform show

```

# 8 - DEFINIR VARIABLES DE ENTRADA

Trabajemos en el mismo directorio de "labs-terraform-gcp" y creamos el archivo 'variables.tf'.
Este archivo define cuatro variables dentro de su configuración de Terraform. Las variables project y credentials_file tienen un bloque vacío: {}. Las variables de región y zona establecen valores predeterminados. Si se establece un valor predeterminado, la variable es opcional. De lo contrario, se requiere la variable. Si ejecuta terraform plan ahora, Terraform le pedirá los valores vacios.

````
variable "project" { }

variable "credentials_file" { }

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-c"
}
````
modifiquemos nuestro main.tf y referenciemos las nuevas variables

````
## Terraform config
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.0.0"
    }
  }
}

## Providers
provider "google" {
  credentials = file(var.credentials_file)  <- Modificar

  project = var.project                     <- Modificar
  region  = var.region                      <- Modificar
  zone    = var.zone                       <- Modificar
}

## Resources
resource "google_compute_network" "vpc_network" {
  name = "my-terraform-network"
}
````

# 9 - Asignar valor a las variables

Puede completar variables utilizando valores de un archivo. Terraform carga automáticamente archivos llamados terraform.tfvars o coincidentes * .auto.tfvars en el directorio de trabajo cuando se ejecutan operaciones.

Creemos un archivo por nombre ```terraform.tfvars```
````
project                  = "<PROJECT_ID>"
credentials_file         = "<FILE>"
````

